module BackupUtils
  def model_filename
    ::File.join(new_resource.directory, 'models', new_resource.name + '.rb')
  end

  def model_file(model_content)
    file model_filename do
      content model_content
      user    new_resource.user
      group   new_resource.group
      mode    0600
    end
  end

  def install_cron
    return if new_resource.cron.nil?

    c = new_resource.cron

    cmd_str = "backup perform --trigger '#{trigger}' --root-path #{new_resource.directory}"
    if new_resource.mute_stdout
      cmd_str << " > /dev/null"
    end

    cron "run_#{new_resource.name}" do
      action      :create
      command     cmd_str
      user        new_resource.user
      minute      minute_setting_or_default
      hour        c[:hour]     if c[:hour]
      mailto      c[:mailto]   if c[:mailto]
      path        c[:path]     if c[:path]
    end
  end

  def trigger
    new_resource.name.gsub(' ', '_')
  end

  def minute_setting_or_default
    # Without specifying minute, we would get one backup per minute!
    new_resource.cron[:minute] || "1"
  end
end
