#
# Cookbook Name:: backup
# Provider:: setup
#
# Author:: Joe Yates <info@bidsnc.com>
#
# Copyright 2013, Joe Yates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

action :create do
  install_dependencies
  create_directories
  config_file
end

def install_dependencies
  %w(libxml2-dev libxslt1-dev liblzma-dev zlib1g-dev).each { |p| package p }

  gem_package 'net-ssh' do
    version '2.9.2'
  end

  gem_package 'backup' do
    version new_resource.backup_gem_version
  end
end

def create_directories
  create_base_directory

  %w(backup log models).each do |p|
    path = ::File.join(new_resource.directory, p)
    directory path do
      user    new_resource.user
      group   new_resource.group
      mode    0700
    end
  end
end

def create_base_directory
  directory new_resource.directory do
    user    new_resource.user
    group   new_resource.group
    mode    0700
  end
end

def config_file
  s = config_content
  file config_filename do
    content s
    user    new_resource.user
    group   new_resource.group
    mode    0600
  end
end

def config_filename
  ::File.join(new_resource.directory, 'config.rb')
end

def config_content
  [
    gpg_config,
    includes_config
  ].join("\n")
end

def gpg_config
  return '' if new_resource.gpg_key.nil? or new_resource.gpg_recipient.nil?

  <<EOT
Backup::Encryptor::GPG.defaults do |encryption|
  encryption.keys = {}
  encryption.keys['#{new_resource.gpg_recipient}'] = <<-KEY
#{new_resource.gpg_key}
  KEY
  encryption.recipients = ['#{new_resource.gpg_recipient}']
end
EOT
end

def includes_config
  <<EOT
# Backup v5.x Configuration
root_path = File.dirname(__FILE__)

Dir["\#{root_path}/models/*.rb"].each do |model|
  instance_eval(File.read(model))
end
EOT
end
