#
# Cookbook Name:: backup
# Provider:: database
#
# Author:: Joe Yates <info@bidsnc.com>
#
# Copyright 2013-4, Joe Yates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include BackupUtils

action :create do
  model_file(model_content)
  install_cron
end

private

def model_content
  s = "Backup::Model.new(:\"#{trigger}\", 'Run backups for a database') do\n"
  s += database_backup_content
  s += "  encrypt_with GPG\n" if new_resource.database_encrypt_gpg
  s += compress_content
  s += store_s3_content
  s += store_local_content
  s += "end"
end

def database_backup_content
  s =  "  database #{database_backup_class} do |db|\n"
  s += "    db.name               = '#{new_resource.database_name}'\n"
  s += "    db.username           = '#{new_resource.database_username}'\n"
  s += "    db.password           = '#{new_resource.database_password}'\n" unless new_resource.database_password.nil?
  s += "    db.host               = '#{new_resource.database_host}'\n" unless new_resource.database_host.nil?
  s += "    db.port               = #{port}\n"
  s += "    db.additional_options = [#{additional_options}]\n"
  s += "  end\n"
end

def database_backup_class
  case
  when new_resource.database_type == "postgresql"
    "PostgreSQL"
  when new_resource.database_type == "mysql"
    "MySQL"
  end
end

def additional_options
  if new_resource.additional_options.size == 0
    ""
  else
    '"' + new_resource.additional_options.join('", "') + '"'
  end
end

def compress_content
  return '' unless new_resource.compress
  <<-EOT
  compress_with Gzip do |compression|
    compression.level = 9
  end
  EOT
end

def store_s3_content
  return '' if new_resource.s3_credentials.nil?

  s3 = new_resource.s3_credentials

  <<-EOT
  store_with S3 do |s3|
    s3.access_key_id     = '#{s3[:access_key]}'
    s3.secret_access_key = '#{s3[:secret_key]}'
    s3.region            = '#{s3[:region]}'
    s3.bucket            = '#{s3[:bucket]}'
    s3.path              = '#{s3[:path]}'
    s3.keep              = #{new_resource.retention}
  end
  EOT
end

def port
  case
  when new_resource.database_port
    new_resource.database_port
  when new_resource.database_type == "postgresql"
    5432
  when new_resource.database_type == "mysql"
    3306
  end
end

def store_local_content
  backups_path = ::File.join(new_resource.directory, 'backup')
  <<-EOT
  store_with Local do |local|
    local.path = '#{backups_path}'
    local.keep = 2
  end
  EOT
end
